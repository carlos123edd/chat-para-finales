const express = require('express');
const morgan = require('morgan');
const socketio = require('socket.io');
const http = require('http');
const mongoose = require('mongoose');
const app = express();
const server = http.createServer(app);
const io = socketio.listen(server);
const path = require('path');

//Coneccion base de datos
 mongoose.connect('mongodb://localhost/chat-database')
    .then(db => console.log('Database connected'))
    .catch(err => console.log(err));

require('./sockets')(io);

app.set("port", process.env.PORT || 3000 );

app.use(morgan("dev"));

app.use(express.static(path.join(__dirname, 'public')));

server.listen(app.get("port"),()=>{
    console.log("Puerto", app.get("port"));
});
